import subprocess

# tool list from here:
# https://github.com/golang/vscode-go/blob/master/extension/tools/allTools.ts.in
#

tools = {
    'gopkgs': 'github.com/uudashr/gopkgs/cmd/gopkgs@v2',
    'go-outline': 'github.com/ramya-rao-a/go-outline@latest',
    'go-symbols': 'github.com/acroca/go-symbols@latest',
    'guru': 'golang.org/x/tools/cmd/guru@latest',
    'gorename': 'golang.org/x/tools/cmd/gorename@latest',
    'gotests': 'github.com/cweill/gotests/gotests@latest',
    'gomodifytags': 'github.com/fatih/gomodifytags@latest',
    'impl': 'github.com/josharian/impl@latest',
    'fillstruct': 'github.com/davidrjenni/reftools/cmd/fillstruct@latest',
    'gopkgs': 'github.com/uudashr/gopkgs/v2/cmd/gopkgs@latest',
    'goplay': 'github.com/haya14busa/goplay/cmd/goplay@latest',
    'godoctor': 'github.com/godoctor/godoctor@latest',
    'godef': 'github.com/rogpeppe/godef@latest',
    'gotype-live': 'github.com/tylerb/gotype-live@latest',
    'godoc': 'golang.org/x/tools/cmd/godoc@latest',
    'gogetdoc': 'github.com/zmb3/gogetdoc@latest',
    'goimports': 'golang.org/x/tools/cmd/goimports@latest',
    'goreturns': 'github.com/sqs/goreturns@latest',
    'goformat': 'winterdrache.de/goformat/goformat@latest',
    'golint': 'golang.org/x/lint/golint@latest',
    'staticcheck': 'honnef.co/go/tools/cmd/staticcheck@latest',
    'golangci-lint': 'github.com/golangci/golangci-lint/cmd/golangci-lint@latest',
    'revive': 'github.com/mgechev/revive@latest',
    'dlv': 'github.com/go-delve/delve/cmd/dlv@latest',
    'godoctor': 'github.com/godoctor/godoctor@latest',
    'gopls': 'golang.org/x/tools/gopls@latest',
    'go-outline':'github.com/ramya-rao-a/go-outline@latest'
}

for tool, url in tools.items():
    print ("install ", tool)
    rc = subprocess.call(["go","install",url])
    if rc != 0:
        print ("FAILED: ", tool)
