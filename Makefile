GITLAB_REGISTRY := registry.gitlab.com

.PHONY: build
build:
	docker build $(USEBUILDCACHE) --pull -t quay.io/ulrichschreiner/vscode-remote-base -t $(GITLAB_REGISTRY)/ulrichschreiner/vscode-remote-images/base .

.PHONY: build-zig
build-zig:
	docker build $(USEBUILDCACHE) \
		--build-arg ZIG_VERSION=unused \
		--build-arg ZLS_VERSION=unused \
		-t quay.io/ulrichschreiner/vscode-zig-remote:latest -t $(GITLAB_REGISTRY)/ulrichschreiner/vscode-remote-images/zig:latest zig

.PHONY: build-go
build-go:
	docker build $(USEBUILDCACHE) -t quay.io/ulrichschreiner/vscode-go-remote -t $(GITLAB_REGISTRY)/ulrichschreiner/vscode-remote-images/go:latest go


.PHONY: build-rust
build-rust:
	docker build $(USEBUILDCACHE) \
		--build-arg RUST_VERSION=1.85.0 \
		--build-arg RUSTUP_VERSION=1.28.1 \
		-t quay.io/ulrichschreiner/vscode-rust-remote:latest -t $(GITLAB_REGISTRY)/ulrichschreiner/vscode-remote-images/rust:latest rust

.PHONY: rust-history
rust-history:
	xdg-open https://rust-lang.github.io/rustup-components-history/

.PHONY: all
all: build build-go build-rust build-zig;

.PHONY: fresh
fresh:
	USEBUILDCACHE="--no-cache" $(MAKE) all

.PHONY: push
push:
	docker push quay.io/ulrichschreiner/vscode-remote-base:latest
	docker push quay.io/ulrichschreiner/vscode-go-remote:latest
	docker push quay.io/ulrichschreiner/vscode-rust-remote:latest
	docker push $(GITLAB_REGISTRY)/ulrichschreiner/vscode-remote-images/base:latest
	docker push $(GITLAB_REGISTRY)/ulrichschreiner/vscode-remote-images/zig:latest
	docker push $(GITLAB_REGISTRY)/ulrichschreiner/vscode-remote-images/go:latest
	docker push $(GITLAB_REGISTRY)/ulrichschreiner/vscode-remote-images/rust:latest
