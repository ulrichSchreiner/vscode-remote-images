# vscode-go-remote

This is my base image for remote development in vscode-go. It installs
many things i need.

You need the following environment variables for this to work:

 - HOSTUSER
 - HOSTUID
 - HOSTGROUP
 - HOSTGID
 - LANG
 - SHELL

These variables are used to add your user with the correct UID/GID to
the remote container and also set the LANGuage and the SHELL. Please note that
you have to add your shell in your Dockerfile if it is not `bash` neither `zsh`.

The `$HOME` of the user in the remote container is set to `/devhome`, so
the extensions and all other things are installed in the container. To mount
your home directory into the remote, take a look at the `.devcontainer/dev-compose.yml`.

## Work

You should create a custom `compose.yml` to abstract away your environment. Take the `.devcontainer/dev-compose.yml` as an example and copy it to your `$HOME/mycompose.yml` and customize your mountpoints. Please note the the $HOME of your user in the remote container is `/devhome` so mounting files like `.gitconfig` should  point to this location.

Now you can create `.devcontainer/devcontainer.json` files in your project like this:

~~~json
{
    "name": "Remote vscode-go",
    "dockerComposeFile": ["${env:HOME}/mycompose.yml"],
    "workspaceFolder": "${env:HOME}/tmp/testxyz",
    "service": "go-dev-tools",
    "extensions": [
        "ms-vscode.Go",
        "humao.rest-client",
        "haaaad.ansible"
    ],
    "overrideCommand": false
}
~~~

Unfortunatly VSCode does not support variables like `${workspaceFolder}`, so
you have to write your workspace folder direct into this file.
