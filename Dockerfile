FROM registry.gitlab.com/ulrichschreiner/base/ubuntu:24.04

LABEL maintainer="ulrich.schreiner@gmail.com"

ENV HELM_VERSION=3.10.0 \
    KUBEFWD_VERSION=1.22.4 \
    LSD_VERSION=1.1.2 \
    KUBIE_VERSION=0.24.0 \
    ZOXIDE_VERSION=0.9.4

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y \
        apt-transport-https \
        bat \
        bc \
        build-essential \
        ca-certificates \
	cmake \
        curl \
        direnv \
        docker.io \
        fzf \
        gcc \
        gettext \
        git \
        gnupg \
        gnupg2 \
	graphviz \
        iproute2 \
        iputils-ping \
        less \
        libssl-dev \
	llvm \
        locales \
        lsof \
        mercurial \
	nodejs \
        openssh-client \
        pkg-config \
	ripgrep \
        sudo \
	telnet \
        tmux \
        tzdata \
        unzip \
        vim \
        wget \
        xz-utils \
        zsh \
    && apt-get clean \
    && rm -rf /var/lib/apt/*

RUN curl -s https://ohmyposh.dev/install.sh >/tmp/install.sh && \
    /bin/bash /tmp/install.sh -d /usr/bin && \
    rm -rf /tmp/install.sh

RUN curl -sSL https://github.com/lsd-rs/lsd/releases/download/v${LSD_VERSION}/lsd_${LSD_VERSION}_amd64.deb >/tmp/lsd.deb \
    && curl -sSL https://github.com/ajeetdsouza/zoxide/releases/download/v${ZOXIDE_VERSION}/zoxide_${ZOXIDE_VERSION}-1_amd64.deb > /tmp/zoxide.deb \
    && dpkg -i /tmp/lsd.deb /tmp/zoxide.deb \
    && rm -rf /var/lib/apt/* /tmp/* /var/tmp/* \
    && curl -sSL https://github.com/sbstp/kubie/releases/download/v${KUBIE_VERSION}/kubie-linux-amd64 >/usr/local/bin/kubie && chmod +x /usr/local/bin/kubie \
    && curl https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz | tar -C /usr/local/bin -xz --strip 1 \
    && curl -sSL https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl \
    && chmod +x /usr/local/bin/kubectl

COPY startup.sh /startup.sh
COPY createUser /tools/createUser

WORKDIR /workspace
CMD [ "/startup.sh" ]
